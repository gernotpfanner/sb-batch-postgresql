package de.localhost.job;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import de.localhost.model.User;
import de.localhost.step.CsvFileReader;
import de.localhost.step.UserProcessor;
import de.localhost.step.UserWriter;

@Configuration
public class WriteJob {
	
	@Value("classpath:record.csv")
	private Resource inputCsv;

	@Bean(name = "csvWriteJob")
	public Job csvWriteJob(JobBuilderFactory jobBuilderFactory,
						   StepBuilderFactory stepBuilderFactory,
						   CsvFileReader csvFileReader,
						   UserProcessor userProcessor,
						   UserWriter userWriter) {

		Step step = stepBuilderFactory.get("step").<User, User> chunk(1)
			.reader(csvFileReader.reader())
			.processor(userProcessor)
			.writer(userWriter).build();

		return jobBuilderFactory.get("writeCsvToHTwo")
				.incrementer(new RunIdIncrementer()).start(step)
				.build();
	}
}
