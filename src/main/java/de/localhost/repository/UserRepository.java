package de.localhost.repository;

import org.springframework.stereotype.Repository;

import de.localhost.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
