package de.localhost.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.localhost.model.User;
import de.localhost.model.UserLog;
import de.localhost.repository.UserLogRepository;
import de.localhost.repository.UserRepository;

@RestController
@PropertySource(value={"classpath:application.properties"})
public class JobLauncherController {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	JobLauncher jobLauncher;
	
	@Autowired 
	@Qualifier("csvWriteJob")
	Job csvWriteJob;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserLogRepository userLogRepository;
	
	@RequestMapping("/showLog")
	public List<UserLog> showLog() {

		return userLogRepository.findAll();
	}
	
	@RequestMapping("/launchPostgresqlJob")
	public List<User> handlePostgresqlJob() {

		try {
			JobParameters jobParameters = new JobParametersBuilder()
					.addLong("time", System.currentTimeMillis())
					.toJobParameters();
			jobLauncher.run(csvWriteJob, jobParameters);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		
		return userRepository.findAll();
	}
}