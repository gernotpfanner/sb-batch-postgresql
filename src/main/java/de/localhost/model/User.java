package de.localhost.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name="users")
public class User {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String userName;
	private String userId;
	private String emailAddress;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL,
			orphanRemoval = true, mappedBy = "user")
	@JsonIgnore
	UserLog userLog;
	
	public User() { }
	
	public User(String userName, String userId, 
			String emailAddress) {
		this.userName = userName;
		this.userId = userId;
		this.emailAddress = emailAddress;
	}
	
	public Integer getId() {
		return id;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}  
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserId() {
		return userId;
	}

	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public UserLog getUserLog() {
		return userLog;
	}

	public void setUserLog(UserLog userLog) {
		this.userLog = userLog;
	}
}

