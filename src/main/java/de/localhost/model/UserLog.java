package de.localhost.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name="users_log")
public class UserLog {
	
	@Id
	private Integer id;
	
	@NotNull
	LocalDateTime changedTime;
	
	@NotNull
	String operation;
	
	@OneToOne
	@JoinColumn(name = "id")
	@MapsId
	User user;

	public UserLog() { }
	
	public UserLog(LocalDateTime changedTime, String operation,
			User user) {
		this.changedTime = changedTime;
		this.operation = operation;
		this.user = user;
	}

	public LocalDateTime getChangedTime() {
		return changedTime;
	}

	public void setChangedTime(LocalDateTime changedTime) {
		this.changedTime = changedTime;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
