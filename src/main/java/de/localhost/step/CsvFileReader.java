package de.localhost.step;

import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.separator.DefaultRecordSeparatorPolicy;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import de.localhost.model.User;

@Component
public class CsvFileReader {

	@Value("classpath:record.csv")
	private Resource inputCsv;
	
	public FlatFileItemReader<User> reader() {
		
		final DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
		tokenizer.setNames("userName", "userId", "emailAddress");
		tokenizer.setDelimiter(";");
		tokenizer.setStrict(false);

		final BeanWrapperFieldSetMapper<User> beanWrapperFieldSetMapper = new BeanWrapperFieldSetMapper<>();
		beanWrapperFieldSetMapper.setTargetType(User.class);

		final DefaultLineMapper<User> lineMapper = new DefaultLineMapper<>();
		lineMapper.setLineTokenizer(tokenizer);
		lineMapper.setFieldSetMapper(beanWrapperFieldSetMapper);

		FlatFileItemReader<User> reader = new FlatFileItemReader<>();
		reader.setResource(inputCsv);
		reader.setRecordSeparatorPolicy(new DefaultRecordSeparatorPolicy());
		reader.setLineMapper(lineMapper);

		return reader;
	}
}
