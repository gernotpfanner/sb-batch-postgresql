package de.localhost.step;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import de.localhost.model.User;
import de.localhost.model.UserLog;
import de.localhost.repository.UserRepository;

@Component
public class UserWriter implements ItemWriter<User> {

	@Autowired
	private UserRepository repo;

	@Override
	@Transactional
	public void write(List<? extends User> users) throws Exception {
		
		for (User user: users) {
			
			user.setUserLog(new UserLog(LocalDateTime.now(), "W", user));
			repo.save(user);
		}
	
	}
}
