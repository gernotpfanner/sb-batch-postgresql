package de.localhost.step;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import de.localhost.model.User;

@Component
public class UserProcessor implements ItemProcessor<User, User> {

	@Override
	public User process(User user) {

		return new User(
				user.getUserName(),
				user.getUserId(),
				user.getEmailAddress());
	}
}

