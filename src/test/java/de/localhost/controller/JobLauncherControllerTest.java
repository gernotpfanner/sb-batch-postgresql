package de.localhost.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.test.MetaDataInstanceFactory;
import org.springframework.batch.test.StepScopeTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import de.localhost.config.DbConfig;
import de.localhost.model.User;
import de.localhost.step.CsvFileReader;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc 
@AutoConfigureEmbeddedDatabase
class JobLauncherControllerTest {

	@Autowired
	private MockMvc mockMvc;
        
	@Autowired
	CsvFileReader csvFileReader;
	
	@MockBean
	private DbConfig postgresqlConfig;

	@Test
	@Tag("Integrationstest")
	void runPostgresRequest() throws Exception {

		this.mockMvc.perform(get("/launchPostgresqlJob"))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$[0].userName", is("bar")));

		this.mockMvc.perform(get("/showLog"))
		.andDo(print()).
		andExpect(status().isOk())
		.andExpect(jsonPath("$[0].user.userName", is("bar")));
	}
	
	
	@Test
	@Tag("Unittest")
	void checkCsvFileReader() throws Exception {

		StepExecution stepExecution = MetaDataInstanceFactory
				.createStepExecution(defaultJobParameters());

		FlatFileItemReader<User> testReader = csvFileReader.reader(); 

		StepScopeTestUtils.doInStepScope(stepExecution, () -> {
			User user;
			int counter = 1;
			testReader.open(stepExecution.getExecutionContext());
			while ((user = testReader.read()) != null) {
				if (counter == 1) {
					assertEquals( "bar", user.getUserName());
				}
				counter++;
			}

			testReader.close();
			return null;
		});
	}
	
	
    private JobParameters defaultJobParameters() {
        JobParametersBuilder paramsBuilder = new JobParametersBuilder();
        return paramsBuilder.toJobParameters();
   }
}
